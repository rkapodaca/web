// Create a function to log the response from the Mandrill API
function log(obj) {
    $('#response').text(JSON.stringify(obj));
}

// create a new instance of the Mandrill class with your API key

var m = new mandrill.Mandrill('6lGmpFKodNkpVp3FVE56gw');
//var m = new mandrill.Mandrill('g9XTJMWDdAMpiumE6b4c4g');
var correoFrom = "rkapodaca@gmail.com";
var correoTo = [
  //{"email":"ingenierouga@gmail.com"},
  {"email":"rkaleb33@gmail.com"}
];

//Parametros de prueba, se sobresscribiran con el formato correcto
var params = "";

var cuerpoCorreo ="";
var nombre = "";
    var ciudad = "";
    var colonia = "";
    var telFijo = "";
    var telMovil = "";
    var correoElectronico = "";
    var tipoCot = "";
    var uno = "";
    var dos = "";
    var tres = "";
    var cuatro = "";
    var consumo1 = "";
    var consumo2 = "";
    var consumo3 = "";
    var consumo4 = "";
    var consumo5 = "";
    var consumo51 = "";
    var consumo52 = "";
    var consumo53 = "";
    var consumo54 = "";
    var consumo55 = "";
    var comentarios = "";
    var largo = "";
    var ancho = "";
    var profundidad = "";
    var volumen = "";
    var temperatura = "";
    var ubicacion = "";
    var factor = "";
    var txtbSisCal = "";
    var txtbCubiertaTermica = "";


function validateForm() {
  window.ParsleyValidator.setLocale('es');
  if ($('form[name="cotizacion"]').parsley().validate()){
    return sendTheMail();
  }else{
    return false;
  }

}


function sendTheMail() {
// Send the email!
    //$('form[name="cotizacion"').parsley();
    //$('.cotizacion').parsley();
    //$cotizacion.parsley().validate();
    // window.ParsleyValidator.setLocale('es');
    // $('form[name="cotizacion"]').parsley().validate;

    inicializaVariables();

    //if(){
      $('input[name="rdioTipoCot"]').is(':checked')

      var correoPlantilla = "<h2>Hay una nueva solicitud de cotizaci\363n</h2><br> \
                    <b>Nombre:</b> "+ nombre +" <br> \
                    <b>Ciudad:</b> "+ ciudad +" <br> \
                    <b>Colonia:</b> "+ colonia +" <br> \
                    <b>Teléfono Fijo:</b> "+ telFijo +" <br> \
                    <b>Teléfono Móvil:</b> "+ telMovil +" <br> \
                    <b>Email:</b> "+ correoElectronico +" <br> \
                    <b>Tipo de Residencia:</b> "+ tipoRes +" <br> \
                    <b>Tipo de Cotización:</b> "+ tipoCot +"<br>";
      var correoCuerpo = "";
    //}
/*
    else{
      alert("Por favor seleccione un tipo de cotizaci\363n");
      return false;
    }
*/
    console.log("sendmail");
    console.log(tipoCot);
    if (tipoCot == "AhorrarLuz") {
        console.log("se usa params 1");

        correoCuerpo =  "<b>Nombre en Recibo:</b> "+ uno +" <br> \
            <b>Número de Servicio:</b> "+ dos +" <br> \
            <b>Tarifa:</b> "+ tres +" <br> \
            <b>Consumo:</b> "+ cuatro +" kWh<br> \
            <b>Detalle de Operaciones:</b> <br> \
              <table><tr><td></td> \
              <th>Consumo</th> \
              <th>Importe</th></tr> \
              <tr><th>Mes 1:</th> \
                  <td align='center'>" + consumo1 +" kWh</td> \
                  <td align='center'>$ "+ consumo51 +" </td> \
              <tr><th>Mes 2:</th> \
                  <td align='center'>"+ consumo2 +" kWh</td> \
                  <td align='center'>$ "+ consumo52 +" </td></tr> \
              <tr><th>Mes 3:</th> \
                  <td align='center'>"+ consumo3 +" kWh</td> \
                  <td align='center'>$ "+ consumo53 +" </td></tr> \
              <tr><th>Mes 4:</th> \
                  <td align='center'>"+ consumo4 +" kWh</td> \
                  <td align='center'>$ "+ consumo54 +" </td></tr> \
              <tr><th>Mes 5:</th> \
                  <td align='center'>"+ consumo5 +" kWh</td> \
                  <td align='center'>$ "+ consumo55 +" </td></tr></table><br>";
    } else if (tipoCot == "AhorrarGas") {
      correoCuerpo = "<b>Tipo de Calentador:</b> "+ tipoCal +" <br> \
      <b>Personas a Dar Servicio:</b> "+ personasServicio +" <br>";

    } else if(tipoCot == "AhorrarAlberca"){
      correoCuerpo = "<b>Largo:</b> "+ largo +" m<br> \
                    <b>Ancho:</b> "+ ancho +" m<br> \
                    <b>Profundidad:</b> "+ profundidad +" m<br> \
                    <b>Volumen:</b> "+ volumen +" m<sup>3</sup><br> \
                    <b>Temperatura:</b> "+ temperatura +" \260C<br> \
                    <b>Ubicación:</b> "+ ubicacion +" <br> \
                    <b>Factor:</b> "+ factor +" <br> \
                    <b>Sistema de Calefacción:</b> "+ sisCal +" <br> \
                    <b>Cubierta Térmica:</b> "+ cubiertaTermica +"<br>";

    } else if(tipoCot == "AhorrarIlum"){
      correoCuerpo ="";
    }

    params = {
      "message": {
        "from_email":correoFrom,
        "to":correoTo,
        "subject": "Cotizaci\363n Habitat Solar",
        "html": correoPlantilla + correoCuerpo + " \
              <b>Comentarios:</b> "+ comentarios +""
      }
    };

    m.messages.send(params, function(res) {
        console.log("exito vato");
        console.log(res);
        log(res);
    }, function(err) {
        console.log("Error");
        console.log(err);
        log(err);
    });

     $("#includedContent").load("mensajeFinal.html");
     return(false);
}


function handleClick(myRadio) {

    if (myRadio.value == "AhorrarLuz" ) {

        tipoCot = "AhorrarLuz";
      $("#includedContent2").load("cotizacion_luz.html");
    }
    if (myRadio.value == "AhorrarGas" ) {

        tipoCot = "AhorrarGas";

      $("#includedContent2").load("cotizacion_gas.html");
    }
    if (myRadio.value == "AhorrarAlberca") {

        tipoCot = "AhorrarAlberca";
      $("#includedContent2").load("cotizacion_alberca.html");
    }
    if (myRadio.value == "AhorrarIlum" ) {

        tipoCot = "AhorrarIlum";
       $("#includedContent2").load("cotizacion_iluminacion.html");
    }
}


function inicializaVariables() {
    var txtbNombre = document.getElementById("txtbNombre");
    nombre = txtbNombre.value;

    var txtbCiudad = document.getElementById("txtbCiudad");
    ciudad = txtbCiudad.value;

    var txtbColonia = document.getElementById("txtbColonia");
    colonia = txtbColonia.value;

    var txtbTelFijo = document.getElementById("txtbTelFijo");
    telFijo = txtbTelFijo.value;

    var txtbTelMovil = document.getElementById("txtbTelMovil");
    telMovil = txtbTelMovil.value;

    var txtbEmail = document.getElementById("txtbEmail");
    correoElectronico = txtbEmail.value;


    tipoRes = $('input[name="tipoRes"]:checked').val();


    var txtbComentarios = document.getElementById("txtbComentarios");

    comentarios = txtbComentarios .value;

    if (tipoCot == "AhorrarLuz" ) {

        var txtbNombreRecibo = document.getElementById("txtbNombreRecibo");
        uno = txtbNombreRecibo.value;

        var txtbServicio = document.getElementById("txtbServicio");
        dos = txtbServicio.value;

        var txtbTarifa = document.getElementById("txtbTarifa");
        tres = txtbTarifa.value;

        var txtbConsumo = document.getElementById("txtbConsumo");
        cuatro = txtbConsumo.value;


        var txtbConsumo1 = document.getElementById("Consumo1");
        consumo1 = txtbConsumo1.value;

        var txtbConsumo2 = document.getElementById("Consumo2");
        consumo2 = txtbConsumo2.value;

        var txtbConsumo3 = document.getElementById("Consumo3");
        consumo3 = txtbConsumo3.value;

        var txtbConsumo4 = document.getElementById("Consumo4");
        consumo4 = txtbConsumo4.value;

        var txtbConsumo5 = document.getElementById("Consumo5");
        consumo5 = txtbConsumo5.value;

         var txtbConsumo51 = document.getElementById("Consumo51");
        consumo51 = txtbConsumo51.value;

        var txtbConsumo52 = document.getElementById("Consumo52");
        consumo52 = txtbConsumo52.value;

        var txtbConsumo53 = document.getElementById("Consumo53");
        consumo53 = txtbConsumo53.value;

        var txtbConsumo54 = document.getElementById("Consumo54");
        consumo54 = txtbConsumo54.value;

        var txtbConsumo55 = document.getElementById("Consumo55");
        consumo55 = txtbConsumo55.value;

    }
    if (tipoCot == "AhorrarGas" ) {

        tipoCal = $('input[name="TipoCal"]:checked').val();

        var txtbPersonasServicio = document.getElementById("txtbPersonasServicio");
        personasServicio = txtbPersonasServicio.value;


    }
    if (tipoCot == "AhorrarAlberca" ) {
         var txtbLargo = document.getElementById("txtbLargo");
        largo = txtbLargo.value;

        var txtbAncho = document.getElementById("txtbAncho");
        ancho = txtbAncho.value;

        var txtbProfundidad = document.getElementById("txtbProfundidad");
        profundidad = txtbProfundidad.value;

        var txtbVolumen = document.getElementById("txtbVolumen");
        volumen = txtbVolumen.value;

        var txtbTemperatura = document.getElementById("txtbTemperatura");
        temperatura = txtbTemperatura.value;

        ubicacion = $('input[name="ubicAlb"]:checked').val();

        factor = $('input[name="viento"]:checked').val();

        sisCal = $('input[name="sisCal"]:checked').val();

        cubiertaTermica = $('input[name="CubiTerm"]:checked').val();


    }

}
